import {LitElement, html} from '@polymer/lit-element'


const userTemplate = (user) => {
  return html`
<style>
    :host {
        display: block;
    }
    
    .user {
        border: 1px solid darkgrey;
        padding: 10px;
    }
</style>
<div class="user">
    <p>Username: ${user.first_name} ${user.last_name}</p>
    <p>Email: ${user.email}</p>
    <p style="color: ${user.fav_color}">Favorite color!</p>
</div>
`
}

class UserComponent extends LitElement {
  static get properties() {
    return {
      userid:  String,
      loading: Boolean,
      error: Boolean
    }
  }

  ready() {
    super.ready()
    this.fetchUser()
  }

  _render({userid, loading, error}) {
    return html`
<style>
    .myclass {
        color: green;
    }
</style>
<button on-click="${() => this.fetchUser()}">fetch a user</button> (see console)
<p>User-id: ${userid}</p>
<p>${loading ? 'Loading...' : 'Loaded!'}</p> 
${ error ? html`<p>An error occured while fetching the user</p>` : ''}
${ !loading && this.user ? userTemplate(this.user) : '' }
`
  }

  fetchUser() {
    this.loading = true
    fetch(`https://my.api.mockaroo.com/users/${this.userid}?key=4ae960c0`)
      .then((res) => { return res.json() })
      .then((data) => { this.user = data })
      .then(() => { this.loading = false })
  }
}

customElements.define('user-component', UserComponent)
