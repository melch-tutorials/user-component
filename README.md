# user-component

Our first Polymer LitElement.

# Setting up the environment

Links:
* [nodejs](https://nodejs.org/en/) - Runtime environment
* [nvm](https://github.com/creationix/nvm) - node version manager
* [npm](https://www.npmjs.com/) - node paclage manager
* [npm - get started](https://docs.npmjs.com/getting-started/what-is-npm) - info about npm / important commands
* [package.json](https://docs.npmjs.com/files/package.json) - package.json documentation
* [semver](https://nodesource.com/blog/semver-a-primer/) - semver: a primer

## Installing nodejs (+ npm)

The runtime environment is nodejs. It can be installed from the homepage (link above). For linux and mac i recommend installation via nvm (node version manager). You can quickly install specific node versions and switch between them. For now normal installation of version 8 will do.

## Create a npm package

We build npm packages (node modules). Every package has a package.json (see docs above) containing name, version, required packages, ....

Important commands:
* `npm init` - Initializes a npm project. Does nothing more than creating a package.json. 
* `npm install` - Installs all packages defined in package.json including their requirements
* `npm install <pkg-name>` - Installs a package (latest version) and adds it to package.json
* `npm install -g <pkg-name>` - Installs a package globally. Allows you to use commands of that package globally in the command line without specifying npm. E.g polymer-cli alows us to use polymer commands like polymer serve.
* `npm run <script-name>` - Runs command specified in package.json under scripts.
* `npm start` - Shortcut to npm run start
* `npm test` - Shortcut to npm run test

npm package version are using semantic versioning (semver). See link above for details. Especially what semver ranges are.

Check if node and npm is correctly installed:
```console
amelch@Alf-Desktop:~$ node -v
v8.11.1
amelch@Alf-Desktop:~$ npm -v
5.8.0
```


# Install polymer-cli

Links:
* [polymer-cli](https://www.npmjs.com/package/polymer-cli) - cli documentation on npm
* [polymer templates](https://github.com/Polymer/tools/tree/master/packages/cli/templates) - Bootstrapping scaffolds
* [paths and names](https://www.polymer-project.org/blog/2018-02-26-3.0-preview-paths-and-names.html) - Polymer Blog: paths and names

`npm install -g polymer-cli@next`

The `@next` flags specifies that we want the preview version. 

**Update**: install polymer with `npm install -g `polymer-cli`

Important commands:
- `polymer init`: Builds up a scaffold to create polymer elements or an app. Explore the options! (letest version was bugged for me. see link for element template above)
- `polymer serve --npm --module-resolution=node` - spins up a local webserver to serve the local files for development.
- **Update**: polymer version ^1.7.2 has `--npm` and `--module-resolution=node` as default. If you installed polymer-cli without the next flag you can just use `polymer serve`

Flags explained:
- `--npm`: Comes from polymers transition from bower to npm. Tells polymer to look in `node_modules`-folder (before `bower_components`) for packages.
- `--module-resolution=node`: Browsers cannot find packages when we declare them like `import {LitElement, html} from '@polymer/lit-element'`. This flag tells polymer to change the name to a relative path when serving files. See the blog post "paths and names" above for more info. 


Check if polymer-cli is installed correctly:

```console

amelch@Alf-Desktop:~$ polymer


   /\˜˜/   /\˜˜/\
  /__\/   /__\/__\    Polymer-CLI
 /\  /   /\  /\  /\
/__\/   /__\/  \/__\  The multi-tool for Polymer projects
\  /\  /\  /   /\  /
 \/__\/__\/   /__\/   Usage: `polymer <command> [options ...]`
  \  /\  /   /\  /
   \/__\/   /__\/


Available Commands

  analyze   Writes analysis metadata in JSON format to standard out             
  build     Builds an application-style project                                 
  help      Shows this help message, or help for a specific command             
  init      Initializes a Polymer project                                       
  install   Installs project dependencies from npm or Bower (optionally         
            installing "variants").                                             
  lint      Identifies potential errors in your code.                           
  serve     Runs the polyserve development server                               
  test      Runs web-component-tester                                           

Global Options

  --env type                      The environment to use to specialize certain  
                                  commands, like build                          
  --entrypoint                    The main HTML file that will be requested for 
                                  all routes.                                   
  --shell string                  The app shell HTML import                     
  --fragment string[]             HTML imports that are loaded on-demand.       
  --root string                   The root directory of your project. Defaults  
                                  to the current working directory.             
  --sources string[]              Glob(s) that match your project source files. 
                                  Defaults to `src/**/*`.                       
  --extra-dependencies string[]   Glob(s) that match any additional             
                                  dependencies not caught by the analyzer to    
                                  include with your build.                      
  --npm                           Sets npm mode: dependencies are installed     
                                  from npm, component directory is              
                                  "node_modules" and the package name is read   
                                  from package.json                             
  --module-resolution string      Algorithm to use for resolving module         
                                  specifiers in import and export statements    
                                  when rewriting them to be web-compatible.     
                                  Valid values are "none" and "node". "none"    
                                  disables module specifier rewriting. "node"   
                                  uses Node.js resolution to find modules.      
  --component-dir string          The component directory to use. Defaults to   
                                  reading from the Bower config (usually        
                                  bower_components/).                           
  -v, --verbose                   turn on debugging output                      
  -h, --help                      print out helpful usage information           
  -q, --quiet                     silence output                                

Run `polymer help <command>` for help with a specific command.


```

Further reading:
- [roadmap 3.0](https://www.polymer-project.org/blog/2018-05-02-roadmap-update.html) - Polymer Blog: About the future of polymer. The future for polymer is what we are using already.
- [webcomponents](https://www.webcomponents.org/introduction) - Webcomponents are the technology polymer is based on. Look at this introduction


# Create our first element

Links:
- [Git](https://git-scm.com/)
- [LitElement](https://github.com/Polymer/lit-element) - Check out documentation
- [lit-html](https://github.com/Polymer/lit-html) - The underlying templating engine

Note about development. If you can: use chrome for development. It will bring all browser functionality needed for custom webcomponents. To support browsers that dont, we will use polyfills (google it). You can see the polyfills script in the demo html template: `<script src="../node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js"></script>`. This script checks the browser functionality and loads only the polyfills needed. So check if the this script is included when developing.

## Using this repo

I will complete this readme later meanwhile you can browse through this repository learn about our first component.

Test out the repo:
```console
amelch@Alf-Desktop:~/code$ git clone https://gitlab.com/qerida/tutorials/user-component.git
Cloning into 'user-component'...
Username for 'https://gitlab.com': alfred.melch@gmx.de
Password for 'https://alfred.melch@gmx.de@gitlab.com': 
remote: Counting objects: 38, done.
remote: Compressing objects: 100% (30/30), done.
remote: Total 38 (delta 15), reused 0 (delta 0)
Unpacking objects: 100% (38/38), done.
Checking connectivity... done.
amelch@Alf-Desktop:~/code$ cd user-component/
amelch@Alf-Desktop:~/code/user-component$ npm install
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN user-component@0.1.0 No repository field.

added 17 packages from 6 contributors in 3.407s
amelch@Alf-Desktop:~/code/user-component$ npm run dev

> user-component@0.1.0 dev /home/amelch/code/user-component
> polymer serve --npm --module-resolution=node

info: [cli.command.serve]    Files in this directory are available under the following URLs
      applications: http://127.0.0.1:8081
      reusable components: http://127.0.0.1:8081/components/user-component/
    
```

I created git tags, so you can see the changes step by step. Use `git tag` to see the tags. (git must be installed)

```console
amelch@Alf-Desktop:~/code/polymer-tests/user-component$ git tag
00-empty-repo
01-scaffold
02-fetchUser
03-loading-state
04-display-user

```

You can see the code from one tag by typing `git checkout <tag-name>`. This way you can browse older states of the repository.

# recommended youtube videos

- [lit-HTML (Chrome Dev Summit 2017)](https://www.youtube.com/watch?v=Io6JjgckHbg) - From Chrome Dev Summit 2017
- [Efficient, Expressive, and Extensible HTML Templates (Polymer Summit 2017)](https://www.youtube.com/watch?v=ruql541T7gc) - From Polymer Summit 2017
- [Practical lessons from a year of building web components - Google I/O 2016](https://www.youtube.com/watch?v=zfQoleQEa4w) - Old but still relevant. (You will see some javascript ;-))
- [What's Next for Polymer (Polymer Summit 2017)](https://www.youtube.com/watch?v=JH6jEcLxJEI) - The transition from bower to npm explained. Also transtítion from 2 to 3
- [Future, Faster: Unlock the Power of Web Components with Polymer (Google I/O '17)](https://www.youtube.com/watch?v=cuoZenpQveQ) - Viel gelaber um devs polymer zu verkaufen. Still a good talk. Bit history. Very good overview.
- [Polymer: Billions Served; Lessons Learned (Google I/O '17)](https://www.youtube.com/watch?v=assSM3rlvZ8) - Lessons learned about Polymer 2. Some code to look at. Not the most interesting video.
- [Building UI at Enterprise Scale with Web Components (Polymer Summit 2017)](https://www.youtube.com/watch?v=FJ2KEvzlyo4) - Very interesting talk from a technical director of EA. How EA uses webcomponents to get a common look and feel across their websites.
